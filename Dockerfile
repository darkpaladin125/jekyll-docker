FROM ruby
WORKDIR /site
RUN gem install bundler jekyll
COPY . .
CMD ["tail", "-f", "/dev/null"]
